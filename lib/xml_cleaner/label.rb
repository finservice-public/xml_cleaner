module XMLCleaner
  class Label
    DEFAULT_FORMAT = ->(size) { "***FILTERED(#{Size.new(size)})***" }

    def initialize(content, format)
      @content = content
      @format = format || DEFAULT_FORMAT
    end

    def to_s
      return format.call(content.bytesize) if format.respond_to?(:call)
      format % [Size.new(content.bytesize)]
    end

    private

    attr_reader :format, :content
  end
end
