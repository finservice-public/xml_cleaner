module XMLCleaner
  class Base
    DEFAULT_ENCODING = 'UTF-8'

    def initialize(options = {})
      @options = default_options.merge(options)
      @paths = filters.map { |f| Path.new(f) }
    end

    def clean(xml)
      return xml if paths.size.zero?
      xml.is_a?(String) ? clean_string(xml) : clean_xml_node(xml.dup)
    end

    private

    attr_reader :options, :paths

    def clean_string(xml)
      document = Nokogiri.XML(xml, nil, options[:encoding], options[:parse_with])
      document = clean_xml_node(document)
      document.to_xml(save_with: options[:save_with])
    rescue Nokogiri::XML::SyntaxError
      xml
    end

    def clean_xml_node(document)
      paths.each do |path|
        document.xpath("//#{path}").each do |node|
          node.content = label(node.content)
        end
      end

      document
    end

    def filters
      options[:filters] || []
    end

    def label(content)
      Label.new(content, options[:format])
    end

    def default_options
      {
        filters: [],
        encoding: DEFAULT_ENCODING,
        parse_with: Nokogiri::XML::ParseOptions::STRICT,
        save_with: Nokogiri::XML::Node::SaveOptions::AS_XML
      }
    end
  end
end
