module XMLCleaner
  class Path
    class << self
      def new(name)
        path = name.is_a?(String) ? name.split('/').reject(&:empty?) : name
        path.map { |key| "*[local-name()='#{key}']" }.join('/')
      end
    end
  end
end
