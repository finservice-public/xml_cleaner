module XMLCleaner
  class Size
    ORDER = 1024
    NAMES = %w(B KB MB GB TB).freeze
    RULES = NAMES.map.with_index { |name, index| [name, ORDER**(index + 1)] }.freeze

    attr_reader :value

    def initialize(value)
      @value = value.to_f
    end

    def to_s
      name, upper_bound = RULES.find { |_name, upper_bound| value < upper_bound } || RULES.last
      "%.2f%s" % [BigDecimal((value / (upper_bound / ORDER)).to_s).truncate(2), name]
    end
  end
end
