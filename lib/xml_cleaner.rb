require 'nokogiri'
require 'bigdecimal'
require 'xml_cleaner/path'
require 'xml_cleaner/base'
require 'xml_cleaner/label'
require 'xml_cleaner/size'
require 'xml_cleaner/version'

module XMLCleaner
  class Error < StandardError; end

  extend self

  def clean(xml, options)
    Base.new(options).clean(xml)
  end
end
