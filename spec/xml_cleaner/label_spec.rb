RSpec.describe XMLCleaner::Label do
  describe '#to_s' do
    subject { described_class.new(content, format).to_s }

    let(:content) { "я" * 150 }

    context 'when format is string' do
      let(:format) { '===(%s)===' }

      it { is_expected.to eq('===(300.00B)===') }
    end

    context 'when format is Proc' do
      let(:format) { ->(size) { "~~~(#{size})~~~" } }

      it { is_expected.to eq('~~~(300)~~~') }
    end
  end
end
