RSpec.describe XMLCleaner::Path do
  describe '.new' do
    subject { described_class.new(filter) }

    let(:filter) { '/Key1/Key2/Key3' }

    it { is_expected.to eq("*[local-name()='Key1']/*[local-name()='Key2']/*[local-name()='Key3']") }
  end
end
