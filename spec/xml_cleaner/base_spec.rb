RSpec.describe XMLCleaner::Base do
  describe '#clean' do
    subject(:result) { described_class.new(options).clean(doc) }

    let(:options) { { filters: filters } }
    let(:filters) { ['/SomeWeirdAction/SomeKey', 'Unexisted/SomeKey', 'SomeWeirdAction/YetAnotherKey'] }
    let(:doc) { xml }
    let(:xml) do
      <<~XML
      <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsdl="http://tempuri.org/">
        <soap:Body>
          <wsdl:SomeWeirdAction>
            <SomeKey>Text Text Text Text...</SomeKey>
            <SomeAnotherKey>Текст в кодировке UTF-8...</SomeAnotherKey>
            <YetAnotherKey>Text Text Text...</YetAnotherKey>
          </wsdl:SomeWeirdAction>
        </soap:Body>
      </soap:Envelope>
      XML
    end

    it { is_expected.to eq(<<~XML
      <?xml version="1.0" encoding="UTF-8"?>
      <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsdl="http://tempuri.org/">
        <soap:Body>
          <wsdl:SomeWeirdAction>
            <SomeKey>***FILTERED(22.00B)***</SomeKey>
            <SomeAnotherKey>Текст в кодировке UTF-8...</SomeAnotherKey>
            <YetAnotherKey>***FILTERED(17.00B)***</YetAnotherKey>
          </wsdl:SomeWeirdAction>
        </soap:Body>
      </soap:Envelope>
    XML
    ) }

    context 'when XML is Nokogiri document' do
      let(:doc) { Nokogiri::XML(xml, nil, 'UTF-8') }

      it { is_expected.to be_an_instance_of(Nokogiri::XML::Document) }
      it { expect(result.to_xml).to eq(<<~XML
        <?xml version="1.0" encoding="UTF-8"?>
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsdl="http://tempuri.org/">
          <soap:Body>
            <wsdl:SomeWeirdAction>
              <SomeKey>***FILTERED(22.00B)***</SomeKey>
              <SomeAnotherKey>Текст в кодировке UTF-8...</SomeAnotherKey>
              <YetAnotherKey>***FILTERED(17.00B)***</YetAnotherKey>
            </wsdl:SomeWeirdAction>
          </soap:Body>
        </soap:Envelope>
      XML
      ) }
    end

    context 'when XML string is invalid' do
      let(:doc) { '<soap:Envelope><soap:Body>' }
      it { expect(result).to eq(doc) }
    end

    context 'when filters option is nil' do
      let(:filters) { nil }
      it { expect(result).to eq(xml) }
    end

    context 'with format as string' do
      let(:options){ { filters: filters, format: "***(%s)***"} }

      it { expect(result).to eq(<<~XML
        <?xml version="1.0" encoding="UTF-8"?>
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsdl="http://tempuri.org/">
          <soap:Body>
            <wsdl:SomeWeirdAction>
              <SomeKey>***(22.00B)***</SomeKey>
              <SomeAnotherKey>Текст в кодировке UTF-8...</SomeAnotherKey>
              <YetAnotherKey>***(17.00B)***</YetAnotherKey>
            </wsdl:SomeWeirdAction>
          </soap:Body>
        </soap:Envelope>
      XML
      ) }
    end

    context 'with format as proc' do
      let(:options){ { filters: filters, format: ->(size){ "~~(#{size})~~" }} }

      it { expect(result).to eq(<<~XML
        <?xml version="1.0" encoding="UTF-8"?>
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsdl="http://tempuri.org/">
          <soap:Body>
            <wsdl:SomeWeirdAction>
              <SomeKey>~~(22)~~</SomeKey>
              <SomeAnotherKey>Текст в кодировке UTF-8...</SomeAnotherKey>
              <YetAnotherKey>~~(17)~~</YetAnotherKey>
            </wsdl:SomeWeirdAction>
          </soap:Body>
        </soap:Envelope>
      XML
      ) }
    end
  end
end
