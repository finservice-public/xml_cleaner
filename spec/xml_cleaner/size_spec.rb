RSpec.describe XMLCleaner::Size do
  subject { described_class.new(value).to_s }

  let(:few_bytes) { XMLCleaner::Size::ORDER - 100 }
  let(:few_kilobytes) { (few_bytes * XMLCleaner::Size::ORDER) + few_bytes }
  let(:few_megabytes) { (few_kilobytes * XMLCleaner::Size::ORDER) + few_kilobytes }
  let(:few_gigabytes) { (few_megabytes * XMLCleaner::Size::ORDER) + few_megabytes }
  let(:few_terabytes) { (few_gigabytes * XMLCleaner::Size::ORDER) + few_gigabytes }
  let(:few_petabytes) { (few_terabytes * XMLCleaner::Size::ORDER) + few_terabytes }

  describe '#to_s' do
    it { expect(described_class.new(few_bytes).to_s).to eq('924.00B')}
    it { expect(described_class.new(few_kilobytes).to_s).to eq('924.90KB')}
    it { expect(described_class.new(few_megabytes).to_s).to eq('925.80MB')}
    it { expect(described_class.new(few_gigabytes).to_s).to eq('926.70GB')}
    it { expect(described_class.new(few_terabytes).to_s).to eq('927.61TB')}
    it { expect(described_class.new(few_petabytes).to_s).to eq('950805.03TB')}
  end
end
