# XMLCleaner

Clean out the content of all listed tags in given XML.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'xml_cleaner', git: 'https://gitlab.com/finservice-public/xml_cleaner.git'
```

And then execute:

```
$ bundle
```

## Usage

```ruby
xml = <<~XML
  <?xml version="1.0" encoding="UTF-8"?>
  <result>
    <ordinalKey>Some data</ordinalKey>
    <largeDataKey>Some other data</largeDataKey>
  </result>
XML

XMLCleaner.clear(xml, filters: ['/result/largeDataKey'], format: '***OMITTED(%s)***')

# Result:
# <?xml version="1.0" encoding="UTF-8"?>
# <result>
#   <ordinalKey>Some data</ordinalKey>
#   <largeDataKey>***OMITTED(15B)***</largeDataKey>
# </result>
```

or

```ruby
cleaner = XMLCleaner::Base.new(filters: ['/result/largeDataKey'], format: '***OMITTED(%s)***')
xml = <<~XML
  <?xml version="1.0" encoding="UTF-8"?>
  <result>
    <ordinalKey>Some data</ordinalKey>
    <largeDataKey>Some other data</largeDataKey>
  </result>
XML
cleaner.clean(xml)

# Result:
# <?xml version="1.0" encoding="UTF-8"?>
# <result>
#   <ordinalKey>Some data</ordinalKey>
#   <largeDataKey>***OMITTED(15B)***</largeDataKey>
# </result>
```

Options are:

- `filters` - It's a list of keys which should be filtered. Default: `[]`.
- `format` - It's a format of a label which will be placed instead of content. It should be a String or Proc object. Example: `"***(%s)***"` => `"***(12MB)***"`.
- `encoding` - It's encoding of XML for parse. Default: `"UTF-8"`
- `parse_with` - It's a parse option. Default: `Nokogiri::XML::ParseOptions::STRICT`.
- `save_with` - It's a parse option. Default: `Nokogiri::XML::Node::SaveOptions::AS_XML`.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
